\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{hyperref}

\usepackage[svgnames]{xcolor}

\lstdefinelanguage{XML}
{
  basicstyle=\ttfamily\footnotesize,
  morestring=[b]",
  moredelim=[s][\bfseries\color{Maroon}]{<}{\ },
  moredelim=[s][\bfseries\color{Maroon}]{</}{>},
  moredelim=[l][\bfseries\color{Maroon}]{/>},
  moredelim=[l][\bfseries\color{Maroon}]{>},
  morecomment=[s]{<?}{?>},
  morecomment=[s]{<!--}{-->},
  commentstyle=\color{DarkOliveGreen},
  stringstyle=\color{blue},
  identifierstyle=\color{red}
}

\lstset{basicstyle=\ttfamily\footnotesize, language=XML}

\title{XML and Web Technologies}
\subtitle{Project Assignment 2: XSLT}
\author{Zhanna Kononenko and Pieter Meiresone\\
\small{Mail: \{Zhanna.Kononenko, pmeireso\}@vub.ac.be}}
\date{May 2015}

\begin{document}

\maketitle

\section{Introduction}
In this project a single XSLT 2.0 stylesheet was created that generates an HTML page for each distinct person (author or editor) that was found in the data from DBLP. In this report we will discuss the most important concepts in our stylesheet.

\section{Implementation}
\subsection{General implementation}
In the overall implementation we took a more procedural approach in solving the problem. We frequently called a template by name instead of using matching. The core of our XSLT is the \texttt{createPersonPage} template to create a page for a person. This template calls two other templates: \texttt{make-overview} to make the table of publications and \texttt{make-co-author-index} to make the co-author index.
\begin{lstlisting}[caption={The template to create a page for a person.}, label={lst:coreFunction}]
<xsl:template name="createPersonPage">
    <xsl:param name="fullName" />

    <html>
        <head><title>Publication of <xsl:value-of select="$fullName"/>
        </title></head>
        <body>
            <xsl:variable name="publications" as="element()*">
                ...
            </xsl:variable>

            <xsl:call-template name="make-overview">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="publications" select="$publications" />
            </xsl:call-template>

            <xsl:call-template name="make-co-author-index">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="publications" select="$publications" />
            </xsl:call-template>
        </body>
    </html>
</xsl:template>
\end{lstlisting}
The entry point of our style sheet is the template that matches with the root. This will create for every author a separate page with the use of the \texttt{xsl:result-document} command and the \texttt{createPersonPage} template. Note that this template uses the \texttt{distinct-values} XPath function, which requires XPath 2.0 and thus XSLT 2.0.
\begin{lstlisting}
<xsl:template match="/">
  <xsl:for-each select="distinct-values(/dblp/*/author|/dblp/*/editor)">
    <xsl:variable name="filePath">
      <xsl:call-template name="get-filepath-from-fullname">
        <xsl:with-param name="fullName" select="." />
      </xsl:call-template>
    </xsl:variable>

    <xsl:result-document href="{$filePath}">
      <xsl:call-template name="createPersonPage">
        <xsl:with-param name="fullName" select="."/>
      </xsl:call-template>
    </xsl:result-document>
  </xsl:for-each>
</xsl:template>
\end{lstlisting}

To improve readability and modularity we have introduced a template to calculate the file path for a certain author, displayed in listing \ref{lst:fnFilePath}. This way we can calculate the file path also in other places in our style sheet, this is especially useful if we have to link to other authors.
\begin{lstlisting}[caption={A function to calculate the file path from a name.}, label={lst:fnFilePath}]
<xsl:template name="get-filepath-from-fullname">
  <xsl:param name="fullName" />

  <!-- Implementation here -->
</xsl:template>
\end{lstlisting}
The stylesheet should generate a file for each distinct person \textit{P} of the form
\begin{center}
    a-tree\texttt{/first-letter-of-lastname/lastname.firstname.html}
\end{center}
However the combination firstname and lastname is not unique to a person. Without any further adjustments we would get an \texttt{XPathException} that says that we could not write more then one result document to the same URI. To fix this we have adapted the file name for a person: if a middle name is available it will be added to the file path. The file path is now of the form
\begin{center}
    a-tree\texttt{/first-letter-of-lastname/lastname.middlename.firstname.html}
\end{center}

\subsection{Overview of the publications from an author}
The overview of the publications from an author is created in the \texttt{make-overview} template. It uses the \texttt{xsl:for-each-group} construct to iterate through all the publications grouped by year.
\\
\\
A requirement was that for each publication a publication number was displayed. This was rather difficult to implement. The earliest publication has publication number 1 and the last publication has the highest number. Furthermore, in the co-author index references should be made to these publication numbers. To implement this we have created a variable that holds all the publications of an author, this was already visible in listing \ref{lst:coreFunction}. The implementation of this variable is visible in listing \ref{lst:publicationVar}. The publications are kept in sorted order, this way we can recalculate the publication number anywhere in the stylesheet. For sorting the \texttt{xsl:perform-sort} construct is used, crucial is the use of \texttt{as="element()*"} in the \texttt{xsl:variable} element. Note that this is only possible with XSLT 2.0.
\begin{lstlisting}[caption={The publication variable.}, label={lst:publicationVar}]
<xsl:variable name="publications" as="element()*">
  <xsl:perform-sort select="$root/dblp/*[author=$fullName]|
  $root/dblp/*[editor=$fullName]">
    <xsl:sort select="year" order="descending" />
    <xsl:sort select="title" order="ascending" />
  </xsl:perform-sort>
</xsl:variable>
\end{lstlisting}
Keeping in mind the publication variable, we can now discuss the important parts of the \texttt{make-overview} template. Everything, except the publication number, are rather trivial so we have omitted these for clarity. Since the \texttt{publications} variable keeps the publications in sorted order, we can retrieve the publication number by using the \texttt{index-of} function.

\begin{lstlisting}
<xsl:template name="make-overview">
  <xsl:param name="fullName" />
  <xsl:param name="publications" />

  <h1><xsl:value-of select="$fullName"/></h1>
  <p>
    <table border="1">
      <xsl:for-each-group select="$publications" group-by="./year">
        <xsl:sort order="descending" select="current-grouping-key()"/>

        <!-- Make table header to indicate year here -->

        <!-- Add table rows to indicate the publications -->
        <xsl:for-each select="current-group()">
          <xsl:sort select="title" order="ascending"/>
          <tr>
            <!-- The publication number -->
            <xsl:variable name="pub-number" 
              select="count($publications) + 1 - index-of($publications, .)[1]" />
            <td align="right" valign="top"><a name="{concat('p', $pub-number)}"/>
              <xsl:value-of select="$pub-number" />
            </td>

            <!-- Link to online version here -->
            <!-- Publication reference here -->
          </tr>
        </xsl:for-each>
      </xsl:for-each-group>
    </table>	
  </p>	
</xsl:template>
\end{lstlisting}

\subsection{Co-author index}
The co-author index is created in the \texttt{make-co-author-index} template. The key in this template is to retrieve the authors with whom the current author has jointly published. Note that we have used a global variable \texttt{root} to keep track of the root. This is necessary because we have to be able to change the context while the current context item is not a node. This happens for example in a \texttt{xsl:for-each} through a list of author names.
\begin{lstlisting}
<xsl:variable name="jointPublishers" select="distinct-values(
  $root/dblp/*/author[preceding-sibling::node() = $fullName]|
  $root/dblp/*/author[following-sibling::node() = $fullName]|
  $root/dblp/*/editor[preceding-sibling::node() = $fullName]|
  $root/dblp/*/editor[following-sibling::node() = $fullName])" />
\end{lstlisting}
Furthermore, for each co-author a link to the corresponding page is present. For all these authors the page will always exist since for each author or editor field of some record in the \texttt{dblp-exeprt.xml} file a HTML page is generated. To get the page address we can use our template that calculates the file path from a name, visible in listing \ref{lst:fnFilePath}.

\section{Conclusion}
Working with XSLT was a very learning experience since various aspects of XML and XPATH are combined. We were also lucky that we were allowed to use XSLT 2.0, otherwise we couldn't use constructs like \texttt{xsl:function}, \texttt{xsl:perform-sort}, \texttt{distinct-values} and \texttt{replace}. This reduced the development time significantly.

\end{document}
